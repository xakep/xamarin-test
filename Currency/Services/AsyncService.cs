﻿namespace Currency
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading.Tasks;

	public class AsyncService : IAsyncService
	{
		private IBanksDataProvider dataProvider;

		public AsyncService(IBanksDataProvider dataProvider)
		{
			this.dataProvider = dataProvider;
		}

		public void Start()
		{
			this.LoadCurrenciesAsync();
		}

		private void LoadCurrenciesAsync() {
			var rand = new Random();

			for (var i = 0; i < this.dataProvider.GetBanks().Count(); i++)
			{
				var bankNumber = i;
				Task.Run(async() =>
					{
						await Task.Delay(rand.Next(2, 7) * 1000);
						this.dataProvider.Update(bankNumber);
					});
			}
		}
	}
}

