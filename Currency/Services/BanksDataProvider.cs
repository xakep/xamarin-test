﻿namespace Currency
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	public delegate void AllCurrenciesLoadedEventHandler(object source, EventArgs e);

	public class BanksDataProvider : IBanksDataProvider
	{
		private int loadedCurrencies = 0;

		private readonly List<Bank> banks = new List<Bank>()
		{
			new Bank("Росгосстрахбанк"),
			new Bank("Рунабанк"),
			new Bank("Русский стандарт"),
			new Bank("СДМ"),
			new Bank("СМП"),
			new Bank("ВТБ"),
			new Bank("Росбанк"),
			new Bank("Банк Москвы"),
			new Bank("Сбербанк"),
			new Bank("Райффайзен")
		};

		public event AllCurrenciesLoadedEventHandler AllCurrenciesLoaded;

		public IEnumerable<Bank> GetBanks()
		{
			return this.banks;
		}

		public void OnAllCurrenciesLoaded(EventArgs e)
		{
			if (this.AllCurrenciesLoaded != null)
			{
				this.AllCurrenciesLoaded(this, e);
			}
		}

		public void Update(int bankNumber)
		{
			var rand = new Random();
			this.banks
				.ElementAt(bankNumber)
				.SetCurrency(new decimal(rand.Next(60, 70) + (float)rand.Next(1, 10) / 10));
			
			if (++this.loadedCurrencies == this.banks.Count)
			{
				this.OnAllCurrenciesLoaded(new EventArgs());
			}
		}

		public decimal GetMaxCurrency()
		{
			return this.banks.Max(bank => bank.Currency);
		}
	}
}

