﻿namespace Currency
{
	using System.Collections.Generic;

	public interface IBanksDataProvider
	{
		/// <summary>
		/// Occurs when all currencies loaded.
		/// </summary>
		event AllCurrenciesLoadedEventHandler AllCurrenciesLoaded;

		/// <summary>
		/// Provides list of available banks.
		/// </summary>
		/// <returns>The banks list.</returns>
		IEnumerable<Bank> GetBanks();

		/// <summary>
		/// Updates the currency for specified bank.
		/// </summary>
		/// <param name="bankNumber">
		/// The bank index in the list.
		/// TODO: the bank should be identified different way.
		/// </param>
		void Update(int bankNumber);

		/// <summary>
		/// Gets the max available currency.
		/// </summary>
		/// <returns>The max currency.</returns>
		decimal GetMaxCurrency();
	}
}

