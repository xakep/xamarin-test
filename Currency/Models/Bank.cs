﻿namespace Currency
{
	using System;
	using System.ComponentModel;

	public class Bank : INotifyPropertyChanged
	{
		private bool isCurrencySet = false;
		public event PropertyChangedEventHandler PropertyChanged;

		public Bank(String name)
		{
			this.Name = name;
			this.Currency = 0;
		}

		public string Name
		{
			get;
			private set;
		}

		public decimal Currency
		{
			get;
			private set;
		}

		public bool IsCurrencySet
		{
			get
			{
				return this.isCurrencySet;
			}

			private set
			{
				if (this.isCurrencySet != value)
				{
					this.isCurrencySet = value;

					if (PropertyChanged != null)
					{
						PropertyChanged(this, new PropertyChangedEventArgs("IsCurrencySet"));
					}
				}
			}
		}

		public void SetCurrency(decimal currency)
		{
			this.Currency = currency;
			this.IsCurrencySet = true;

			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs("Currency"));
			}
		}
	}
}

