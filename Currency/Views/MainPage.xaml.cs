﻿namespace Currency
{
	using System;
	using System.Collections.Generic;
	using Xamarin.Forms;

	public partial class MainPage : ContentPage
	{
		public MainPage(IBanksPresenter presenter)
		{
			InitializeComponent();
			BindingContext = presenter;
		}
	}
}

