﻿namespace Currency
{
	using System;
	using Microsoft.Practices.Unity;
	using Xamarin.Forms;

	public class App : Application
	{
		public App()
		{
			IUnityContainer container = new UnityContainer();
			container.RegisterType<IBanksDataProvider, BanksDataProvider>(
				new ContainerControlledLifetimeManager());
			container.RegisterType<IBanksPresenter, BanksPresenter>();
			container.RegisterType<IAsyncService, AsyncService>();

			MainPage = container.Resolve<MainPage>();
			container.Resolve<IAsyncService>().Start();
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

