﻿namespace Currency
{
	using System.Collections.Generic;
	using System.Windows.Input;

	public interface IBanksPresenter
	{
		IEnumerable<Bank> Banks { get; }
		bool IsDataAvailable { get; }
		ICommand OpenResults { get; }
		string Result { get; }
		string UserInput { get; set; }
	}
}

