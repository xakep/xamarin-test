﻿namespace Currency
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Linq;
	using System.Windows.Input;
	using Xamarin.Forms;

	public class BanksPresenter : IBanksPresenter, INotifyPropertyChanged
	{
		private IBanksDataProvider dataProvider;
		private bool isAllDataAvailable = false;
		private string result = string.Empty;

		public event PropertyChangedEventHandler PropertyChanged;

		public BanksPresenter(IBanksDataProvider dataProvider)
		{
			this.dataProvider = dataProvider;
			this.dataProvider.AllCurrenciesLoaded += this.AllCurrenciesLoaded;
		}

		public IEnumerable<Bank> Banks
		{
			get
			{
				return this.dataProvider.GetBanks()
					.OrderByDescending(bank => bank.Currency);
			}
		}

		public bool IsDataAvailable
		{
			get
			{
				return this.isAllDataAvailable;
			}

			private set
			{
				this.isAllDataAvailable = value;
				this.RaisePropertyChanged("IsDataAvailable");
			}
		}

		public ICommand OpenResults
		{
			get
			{
				return new Command(() => {
					decimal amount;
					var conversionResult = decimal.TryParse(this.UserInput, out amount);
					if (conversionResult)
					{
						this.Result = string.Format(
							"Максимальная сумма {0:0.00} рублей",
							this.dataProvider.GetMaxCurrency() * amount);
					}

					this.UserInput = string.Empty;
					this.RaisePropertyChanged("UserInput");
				});
			}
		}

		public string Result
		{
			get
			{
				return this.result;
			}

			private set
			{
				this.result = value;
				this.RaisePropertyChanged("Result");
			}
		}

		public string UserInput
		{
			get;
			set;
		}

		private void AllCurrenciesLoaded (object source, EventArgs e)
		{
			this.IsDataAvailable = true;
			this.RaisePropertyChanged("Banks");
		}

		private void RaisePropertyChanged(string property)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}
	}
}

